FROM node:20-bullseye

# gcc for cgo AND nodejs & npm
RUN apt-get -qq update \
  && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends \
    g++ \
    gcc \
    libc6-dev \
    make \
    pkg-config \
    wget \
  && rm -rf /var/lib/apt/lists/*

ENV PATH /usr/local/go/bin:$PATH

ENV GOLANG_VERSION 1.22.3
ENV GOLANG_REL_ARCH linux-amd64

RUN set -eux; \
  url="https://golang.org/dl/go${GOLANG_VERSION}.${GOLANG_REL_ARCH}.tar.gz"; \
  wget -O go.tgz "$url" --progress=dot:giga; \
  tar -C /usr/local -xzf go.tgz; \
  rm go.tgz;

ENV GOPATH /go
ENV PATH $GOPATH/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH
